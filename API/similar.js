/*
###Google###
semelhante
Retorna uma lista de aplicativos semelhantes ao especificado. Opções:

appId: o ID do Google Play do aplicativo para o qual obter aplicativos semelhantes.
lang (opcional, o padrão é 'en'): o código do idioma de duas letras usado para recuperar os aplicativos.
país (opcional, o padrão é 'us'): o código do país com duas letras usado para recuperar os aplicativos.
fullDetail (opcional, o padrão é false): se verdadeiro, uma solicitação extra será feita para cada aplicativo resultante para buscar todos os detalhes.

###APPLE###
semelhante
Retorna a lista de aplicativos "os clientes também compraram" mostrados na página de detalhes do aplicativo. Opções:

id: o iTunes "trackId" do aplicativo, por exemplo, 553834731 para Candy Crush Saga. Isso ou o appId devem ser fornecidos.
appId: o "bundleId" do aplicativo do iTunes, por exemplo, com.midasplayer.apps.candycrushsaga para Candy Crush Saga. Este ou o ID deve ser fornecido.

*/
const infos_global = require('./const_global');
const gplay = require(infos_global() + "/google-play-scraper");
const applay = require(infos_global() + "/app-store-scraper");

class __similar {
    constructor(ID, Path) {
        this.IdApp = ID;
        this.PathApi = Path;
    }
    get_similar() {
        if (this.PathApi == 'google') {
            var __simi = gplay.similar({ appId: String(this.IdApp), fullDetail: true })
            .then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado Apps Similares para o apppID: "'+ this.IdApp+'" na Google Play Store.';}
                  else{
                return params}
            }).catch((failureCallback) => {
                return'Não encontrado Apps Similares para o apppID: "'+ this.IdApp+'" na Google Play Store.';}
            );
            return __simi;
        } else if (this.PathApi == 'apple') {
            var __simi = applay.similar({ id: String(this.IdApp) })
            .then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado Apps Similares para o apppID: "'+ this.IdApp+'" na Apple Store.';}
                  else{
                return params}
            }).catch((failureCallback) => {
                return'Não encontrado Apps Similares para o apppID: "'+ this.IdApp+'" na Apple Store.';}
            );
            return __simi;
        }
    }
}

module.exports = __similar