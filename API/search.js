/*###Google###
procurar
Recupera uma lista de aplicativos que resultam da pesquisa pelo termo especificado. Opções:

termo: o termo a ser pesquisado.
num (opcional, o padrão é 20, o máximo é 250): a quantidade de aplicativos a serem recuperados.
lang (opcional, o padrão é 'en'): o código do idioma de duas letras usado para recuperar os aplicativos.
país (opcional, o padrão é 'us'): o código do país com duas letras usado para recuperar os aplicativos.
fullDetail (opcional, o padrão é false): se verdadeiro, uma solicitação extra será feita para cada aplicativo resultante para buscar todos os detalhes.
preço (opcional, padrão para todos): permite controlar se os aplicativos de resultados são gratuitos, pagos ou ambos.
tudo: Gratuito e pago
grátis: apenas aplicativos gratuitos
pago: somente aplicativos pagos 

###APPLE###

Recupera uma lista de aplicativos que resultam da pesquisa pelo termo especificado. Opções:

term: o termo a ser pesquisado (obrigatório).
num: a quantidade de elementos a serem recuperados. O padrão é 50.
página: página de resultados a serem recuperados. O padrão é 1.
país: o código do país com duas letras para obter aplicativos semelhantes. O padrão é para nós.
lang: código do idioma para o texto do resultado. O padrão é en-us.
idsOnly: (opcional, o padrão é false): pula a solicitação de pesquisa extra. Os resultados da pesquisa conterão uma variedade de IDs de aplicativos.
*/


const infos_global = require('./const_global');
const gplay = require(infos_global() + "/google-play-scraper");
const applay = require(infos_global() + "/app-store-scraper");

class __search {

    constructor(Termo, Path) {

        this.termo = Termo;
        this.PathApi = Path;
    }
    lista_app() {
        if (this.PathApi == 'google') {
            var __pesquisa = gplay.search({
                    term: String(this.termo),
                    num: 100,
                    fullDetail: true
                }).then(function ret(params) {
                    if(params.leng == 0){
                      return'Não encontrado o termo em aplicativos da Google Play Store.'   
                    }else{
                    return params}
                }).catch((failureCallback) => {
                    return 'Não encontrado o termo em aplicativos da Google Play Store.'
                });
                return __pesquisa;
            
        } else if (this.PathApi == 'apple') {

            var __pesquisa=applay.search({
                term: String(this.termo),
                num: 100
              }).then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado o termo em aplicativos da Apple Store.'   
                }else{
                return params}
            }).catch((failureCallback) => {
                return 'Não encontrado o termo em aplicativos da Apple Store.'
            });
              return __pesquisa;
        }
    }
}

module.exports = __search
