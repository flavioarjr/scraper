/*
###Google###
Métodos disponíveis:

-->app: recupera todos os detalhes de um aplicativo.
**lista: recupera uma lista de aplicativos de uma das coleções no Google Play.
-->pesquisa: recupera uma lista de aplicativos que resultam da pesquisa pelo termo especificado.
-->desenvolvedor: retorna a lista de aplicativos pelo nome de desenvolvedor fornecido.
**sugerir: uma string retorna até cinco sugestões para concluir um termo de consulta de pesquisa.
-->reviews: Recupera uma página de reviews para um aplicativo específico.
-->similar: retorna uma lista de aplicativos semelhantes ao especificado.
-->permissões: retorna a lista de permissões às quais um aplicativo tem acesso.
**categorias: recupere uma lista completa de categorias presentes no menu suspenso no Google Play. 


###Apple###

Métodos disponíveis:

-->app: recupera todos os detalhes de um aplicativo.
**lista: recupera uma lista de aplicativos de uma das coleções no iTunes.
-->pesquisa: recupera uma lista de aplicativos que resultam da pesquisa pelo termo especificado.
-->desenvolvedor: recupera uma lista de aplicativos pelo ID de desenvolvedor fornecido.
**sugerir: uma string retorna até 50 sugestões para concluir um termo de consulta de pesquisa.
-->similar: retorna a lista de aplicativos "os clientes também compraram" mostrados na página de detalhes do aplicativo.
-->reviews: recupera uma página de reviews para o aplicativo.
**classificações: recupera as classificações do aplicativo.

*/


const infos_global = require('./const_global');
const express_lib = require(infos_global() + '/express');
var express_methods = express_lib()



express_methods.get("/", function (req, res) {
    res.sendFile(__dirname+"/html/home.html");
})


//Retornar informações de todos os apps disponiveis
//-->Criar "cadastro de Apps em Banco" e fazer select no app.js
//###Get_API - Google
express_methods.get("/api/google/app/:app", function (req, res) {
    const app_info = require('./app.js');
    const info = new app_info( req.params.app,req.path.split("/")[2]);
    info.get_app().then(res.send.bind(res));
})
//###Get_API - Apple
express_methods.get("/api/apple/app/:app", function (req, res) {
    const app_info = require('./app.js');
    const info = new app_info( req.params.app,req.path.split("/")[2]);
    info.get_app().then(res.send.bind(res));
})

//Retorna todos os aplicativos que apresentem em seu contexto os termos:
// "itaú" || "Itaú" || "Itau" || "itau" || "unibanco" || "Unibanco"
// --> Criar Mecanismo de Autenticação e validar se o app é do itau ou não
//###Get_API - Google
express_methods.get("/api/google/search/:term", function (req, res) {
    const app_info = require('./search.js');
    const lista_search = new app_info( req.params.term,req.path.split("/")[2]);
    lista_search.lista_app().then(res.send.bind(res));
})
//###Get_API - APPLE
express_methods.get("/api/apple/search/:term", function (req, res) {
    const app_info = require('./search.js');
    const lista_search = new app_info( req.params.term,req.path.split("/")[2]);
    lista_search.lista_app().then(res.send.bind(res));
})

//Retorna todos os aplicativos desenvolvidos por um IDdeveloper vinculado ou Itau Unibanco
//Criar "Cadastro de Developers em banco " e fazer select na função developer.js
//###Get_API - Google
express_methods.get("/api/google/developer/:devID", function (req, res) {
    const app_dev = require('./developer.js');
    const lista_appsByDev = new app_dev( req.params.devID,req.path.split("/")[2]);
    lista_appsByDev.appsByDev().then(res.send.bind(res));
})
//###Get_API - APPLE
express_methods.get("/api/apple/developer/:devID", function (req, res) {
    const app_dev = require('./developer.js');
    const lista_appsByDev = new app_dev( req.params.devID,req.path.split("/")[2]);
    lista_appsByDev.appsByDev().then(res.send.bind(res));
})

//Retorna Reviews de apps com fulldatils
//###Get_API - Google
express_methods.get("/api/google/reviews/:app", function (req, res) {
    const app_revw = require('./reviews.js');
    const __revws = new app_revw(req.params.app, req.path.split("/")[2]);
    __revws.get_reviews().then(res.send.bind(res));
})
//###Get_API - APPLE
express_methods.get("/api/apple/reviews/:app", function (req, res) {
    const app_revw = require('./reviews.js');
    const __revws = new app_revw(req.params.app, req.path.split("/")[2]);
    __revws.get_reviews().then(res.send.bind(res));
})


//Retorna conjuto de apps que possuem alguma similaridade com o app passado
//modificar o método para receber um app como parametro
//###Get_API - Google
express_methods.get("/api/google/similar/:id", function (req, res) {
    console.log(req.params.id, req.path.split("/")[2]);
    const list_appSimilar = require('./similar.js');
    const __listApps = new list_appSimilar(req.params.id, req.path.split("/")[2]);
    __listApps.get_similar().then(res.send.bind(res));
})
//###Get_API - Apple/
express_methods.get("/api/apple/similar/:id", function (req, res) {
    console.log(req.params.id, req.path.split("/")[2]);
    const list_appSimilar = require('./similar.js');
    const __listApps = new list_appSimilar(req.params.id, req.path.split("/")[2]);
    __listApps.get_similar().then(res.send.bind(res));
})

//Retorna conjuto de apps que possuem alguma similaridade com o app passado
express_methods.get("/api/google/permissions/:app", function (req, res) {
    require('./permissions.js')(String(req.params.app)).then(res.send.bind(res));
})
//const porta = process.env.PORT;
var porta = process.env.PORT || 5000;
express_methods.listen(porta, function () {
    console.log("Servidor em Operação na porta:"+porta+".")
});
