/*
###Google###
avaliações
Recupera uma página de revisões para um aplicativo específico.

Observe que esse método retorna avaliações em um idioma específico (inglês por padrão), portanto, é necessário tentar idiomas diferentes para obter mais avaliações. Além disso, o contador exibido na página do Google Play refere-se ao número total de classificações de 1 a 5 estrelas que o aplicativo possui, não a contagem de comentários escritos. Portanto, se o aplicativo tiver 100 mil classificações, não espere receber 100 mil críticas usando esse método.

Opções:

appId: ID exclusivo do aplicativo para o Google Play. (por exemplo, id = com.mojang.minecraftpe mapeia o jogo Minecraft: Pocket Edition).
lang (opcional, o padrão é 'en'): o código de idioma de duas letras no qual buscar as revisões.
país (opcional, o padrão é 'us'): o código do país com duas letras para buscar os comentários.
sort (opcional, o padrão é sort.NEWEST): a maneira como as avaliações serão classificadas. Os valores aceitos são: sort.NEWEST, sort.RATING e sort.HELPFULNESS.
num (opcional, o padrão é 100): quantidade de comentários a serem capturados.


###APPLE###

Recupera uma página de comentários para o aplicativo. Opções:

id: o iTunes "trackId" do aplicativo, por exemplo, 553834731 para Candy Crush Saga. Isso ou o appId devem ser fornecidos.
appId: o "bundleId" do aplicativo do iTunes, por exemplo, com.midasplayer.apps.candycrushsaga para Candy Crush Saga. Este ou o ID deve ser fornecido.
país: o código do país com duas letras para obter as críticas. O padrão é para nós.
page: o número da página de revisão a ser recuperada. O padrão é 1, o máximo permitido é 10.
sort: a ordem de classificação da revisão. O padrão é store.sort.RECENT, as opções disponíveis são store.sort.RECENT e store.sort.HELPFUL.


*/

const infos_global = require('./const_global');
const gplay = require(infos_global() + "/google-play-scraper");
const applay = require(infos_global() + "/app-store-scraper")
class __reviews{
    constructor(ID,Path){
        this.IdApp = ID;
        this.PathApi = Path;
    }
    get_reviews(){
        if(this.PathApi=='google'){
            var __revw = gplay.reviews({
                appId: String(this.IdApp),
                sort: gplay.sort.NEWEST,
                num: 100,
                fullDetail: true
            }).then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado Reviews para o apppID: "'+ this.IdApp+'" na Google Play Store.';}
                  else{
                return params}
            }).catch((failureCallback) => {
                return'Não encontrado Reviews para o apppID: "'+ this.IdApp+'" na Google Play Store.';}
            );
            return __revw;

        }else if(this.PathApi == 'apple'){
            var __revw = applay.reviews({
                appId: String(this.IdApp),
                sort: applay.sort.NEWEST,
                page: 10
              }).then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado Reviews para o apppID: "'+ this.IdApp+'" na Apple Store.';}
                  else{
                return params}
            }).catch((failureCallback) => {
                return'Não encontrado Reviews para o apppID: "'+ this.IdApp+'" na Apple Store.';}
            );
            return __revw;
        }
    }
}
module.exports = __reviews