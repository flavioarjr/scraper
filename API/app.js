/* ###GOOGLE###
Recupera todos os detalhes de um aplicativo. Opções:

appId: o ID do Google Play do aplicativo (o parâmetro? id = no URL).
lang (opcional, o padrão é 'en'): o código do idioma de duas letras no qual buscar a página do aplicativo.
país (opcional, o padrão é 'us'): o código do país com duas letras usado para recuperar os aplicativos. Necessário quando o aplicativo está disponível apenas em alguns países. 

  ###APPLE###

Recupera todos os detalhes de um aplicativo. Opções:

id: o iTunes "trackId" do aplicativo, por exemplo, 553834731 para Candy Crush Saga. Isso ou o appId devem ser fornecidos.
appId: o "bundleId" do aplicativo do iTunes, por exemplo, com.midasplayer.apps.candycrushsaga para Candy Crush Saga. Este ou o ID deve ser fornecido.
país: o código do país com duas letras para obter os detalhes do aplicativo. O padrão é para nós. Observe que isso também afeta o idioma dos dados.
ratings: carrega informações adicionais sobre ratings, como número de classificações e histograma

*/

const infos_global = require('./const_global');
const gplay = require(infos_global() + "/google-play-scraper");
const applay = require(infos_global() + "/app-store-scraper")

class __appInfo {
  constructor(IdApp, Path) {
    this.IdAplicativo = IdApp;
    this.PathApi = Path;
  }
  get_app() {
    if (this.PathApi == 'google') {
      var __appInforma = gplay.app({ appId: String(this.IdAplicativo) })
        .then(function ret(params) {
          return params
        }).catch((failureCallback) => {
          return 'Aplicativo Não Encontrado na Google Play Store.'
      });
      return __appInforma;

    }
    else if (this.PathApi == 'apple') {
      var __appInforma = applay.app({ id: String(this.IdAplicativo), ratings: true })
        .then(function ret(params) {
          return params
        }).catch((failureCallback) => {
          return 'Aplicativo Não Encontrado na Apple Store.'
      });
      return __appInforma;
    }
  }
}

module.exports = __appInfo
