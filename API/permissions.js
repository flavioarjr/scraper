/*
permissões
Retorna a lista de permissões às quais um aplicativo tem acesso.

appId: o ID do Google Play do aplicativo para o qual obter permissões.
lang (opcional, o padrão é 'en'): o código do idioma de duas letras no qual buscar as permissões.
curto (opcional, o padrão é false): se verdadeiro, os nomes das permissões serão retornados em vez dos objetos de permissão / descrição.
*/
const infos_global = require('./const_global');
const gplay = require(infos_global() + "/google-play-scraper");

var __permission = function get_reviews(IdApp) {
    var __permis = gplay.permissions({ appId: IdApp })
        .then(function ret(params) {
            return params
        })
          
    return __permis
}
module.exports = __permission