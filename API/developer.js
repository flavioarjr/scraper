/*
###GOOGLE###
developer
Retorna a lista de aplicativos pelo nome de desenvolvedor fornecido. Opções:

    devId: o nome do desenvolvedor.
    lang (opcional, o padrão é 'en'): o código do idioma de duas letras no qual buscar a lista de aplicativos.
    país (opcional, o padrão é 'us'): o código do país com duas letras usado para recuperar os aplicativos. Necessário quando o aplicativo está disponível apenas em alguns países.
    num (opcional, o padrão é 60): a quantidade de aplicativos a serem recuperados.
    fullDetail (opcional, o padrão é false): se verdadeiro, uma solicitação extra será feita para cada aplicativo resultante para buscar todos os detalhes.

###APPLE###
Recupera uma lista de aplicativos pelo ID do desenvolvedor fornecido. Opções:

    devId: o iTunes "artistId" do desenvolvedor, por exemplo 284882218 para o Facebook.
    país: o código do país com duas letras para obter os detalhes do aplicativo. O padrão é para nós. Observe que isso também afeta o idioma dos dados.
*/
//imports
const infos_global = require('./const_global');
const gplay = require(infos_global() + "/google-play-scraper");
const applay = require(infos_global() + "/app-store-scraper");
class __dev{
    constructor(IdDeveloper, Path) {

        this.DevId = IdDeveloper;
        this.PathApi = Path;
    }
    appsByDev(){
        if(this.PathApi == 'google'){
            var __developer = gplay.developer({ devId: String(this.DevId), fullDetail: true })
            .then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado Apps desenvolvidos por: "'+   String(this.DevId) + '" na Google Play Store.';                }else{
                return params}
            }).catch((failureCallback) => {
                return'Não encontrado Apps desenvolvidos por: "'+   String(this.DevId) + '" na Google Play Store.';
            });            
                return __developer;
        }else if (this.PathApi == 'apple'){
            var __developer = applay.developer({devId: String(this.DevId),ratings: true })
            .then(function ret(params) {
                if(params.length == 0){
                  return'Não encontrado Apps desenvolvidos por: "'+   String(this.DevId) + '" na Apple Store.';                
                }else{
                return params}
            }).catch((failureCallback) => {
                return'Não encontrado Apps desenvolvidos por: "'+   String(this.DevId) + '" na Apple Store.'
            });            
                        
        return __developer;
        }
    }
}
module.exports = __dev
